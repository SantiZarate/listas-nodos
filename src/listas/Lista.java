package listas;

public class Lista {
Nodo primero;
	
	public void agregarAdelante(int i) {
		Nodo nuevo = new Nodo();
		nuevo.elemento = i;
		nuevo.siguiente = this.primero;
		this.primero = nuevo;
	}
	
	public void mostrar() {
		Nodo iterador = this.primero;
		
		while(iterador != null) {
			System.out.print(iterador.elemento + " ");
			iterador = iterador.siguiente;
		}
		
		System.out.println();
	}
	
	//EJERCICIO NUMERO 1
	public void descomponerPares() {
		Nodo iterador = this.primero;
		
		while(iterador != null) {
			if(iterador.elemento % 2 == 0) {
				Nodo nuevo = new Nodo();
				nuevo.elemento = iterador.elemento * 2;
				iterador.elemento = iterador.elemento / 2;
				
				nuevo.siguiente = iterador.siguiente;
				iterador.siguiente = nuevo;
				iterador = iterador.siguiente;
			}
			
			iterador = iterador.siguiente;
		}
	}
	
	//EJERCICIO NUMERO 2
	public void filtrarElementosEnRango(int k, int m) {
		Nodo iterador = this.primero;
		
		while(iterador != null && iterador.siguiente != null) {
			if(iterador.siguiente.elemento < k || iterador.siguiente.elemento > m) {
				iterador.siguiente = iterador.siguiente.siguiente;
			}else {
				//Solo avanzamos si no borro
				iterador = iterador.siguiente;
			}
		}
		//Elimino el primero despues de recorrer los nodos
		if(this.primero.elemento  < k || this.primero.elemento > m ) {
			this.primero = this.primero.siguiente;
		}
	}
	
	
	//EJERCICO NUMERO 3
	public Lista extraerPares() {
		Lista listaPares = new Lista();
		Nodo iterador = this.primero;
		
		while(iterador != null && iterador.siguiente != null) {
			if(iterador.siguiente.elemento % 2 == 0) {
				listaPares.agregarAdelante(iterador.siguiente.elemento);
				iterador.siguiente = iterador.siguiente.siguiente;
			} else {
				iterador = iterador.siguiente;
			}
		}
		//Elimino el primero despues de recorrer los nodos
		if(this.primero.elemento  % 2 == 0 ) {
			this.primero = this.primero.siguiente;
		}
		
		return listaPares;
	}
}
