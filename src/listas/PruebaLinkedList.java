package listas;

import java.util.LinkedList;

public class PruebaLinkedList {

	public static void main(String[] args) {
		LinkedList <Integer> listaEntero = new LinkedList <Integer>();
		
		//listaEntero.add(14);
		//listaEntero.add(19);
		//listaEntero.add(32);
		
		while(listaEntero.size() < 5) {
			int enteroRandom = (int) (Math.random()*50.0);
			
			if(!listaEntero.contains(enteroRandom)) {
				listaEntero.add(enteroRandom);
			}
		}
		System.out.println(listaEntero);
	}
}