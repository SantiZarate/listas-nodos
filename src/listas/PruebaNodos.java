package listas;

public class PruebaNodos {

	public static void main(String[] args) {
		Lista lista1 = new Lista();
		
		//AGREGAMOS ELEMENTOS PARAR PODER VER COMO FUNCIONAN LOS METODOS
		lista1.agregarAdelante(3);
		lista1.agregarAdelante(12);
		lista1.agregarAdelante(9);
		lista1.agregarAdelante(20);
		lista1.agregarAdelante(8);
		lista1.agregarAdelante(2);
		
		lista1.mostrar();
		
//		lista1.filtrarElementosEnRango(4, 12);
		
		lista1.extraerPares().mostrar();
		lista1.mostrar();
	}
}